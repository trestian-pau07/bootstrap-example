package com.sda.trestian.springboot.springboottest.repository;

import com.sda.trestian.springboot.springboottest.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
}
