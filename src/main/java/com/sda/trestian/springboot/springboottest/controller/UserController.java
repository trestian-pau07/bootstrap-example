package com.sda.trestian.springboot.springboottest.controller;

import com.sda.trestian.springboot.springboottest.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class UserController {

    @GetMapping("/users/user")
    public String getUsersView(Model model){
        List<User> users = new ArrayList<>();
        User user = new User();
        users.add(user);
        model.addAttribute("userr",new User());
        model.addAttribute("users", users);
        List<String> userRoles = SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        model.addAttribute("authenticatedUserRoles",userRoles);
        return "users";
    }

    @PostMapping("/users/user")
    public String createUser(User userr, Model model){
        model.addAttribute("users", null);
        model.addAttribute("userr", new User());
        return "users";
    }
    @GetMapping
    public String getHome(){
        return "home";
    }

    @GetMapping("/hello")
    public String getHello(){
        return "hello";
    }
}
