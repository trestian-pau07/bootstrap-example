package com.sda.trestian.springboot.springboottest.service;

import com.sda.trestian.springboot.springboottest.model.User;
import com.sda.trestian.springboot.springboottest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }
}
